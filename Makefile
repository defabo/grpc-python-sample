.PHONY: proto
proto:
	python run_codegen.py

.PHONY: clean
clean:
	rm sample_pb2*
