import argparse
import logging

import grpc

from sample_pb2 import ConvertRequest, ConvertReply
from sample_pb2_grpc import SampleServiceStub


def convert(stub: SampleServiceStub, text: str) -> ConvertReply:
    return stub.convert(ConvertRequest(text=text))


def run(args: argparse.Namespace) -> None:
    logging.basicConfig()
    with grpc.insecure_channel(args.host) as channel:
        stub = SampleServiceStub(channel)
        reply = convert(stub, args.text)
        print(f"{args.text} -> {reply.text}")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", default="localhost:50051", help="gRPC server address and port, default: %(default)s")
    parser.add_argument("text", help="text to send to remote service")
    run(parser.parse_args())
