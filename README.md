# grpc-python-sample

gRPC sample for use as a base template for Defabo gRPC based python services. Loosely based on gRPC python examples from [www.grpc.io](https://www.grpc.io/docs/tutorials/basic/python/) 

This is basic sample covering only basics. Extend it according to following rules:

- All code must be compatible with Python 3.6.8
- If possible prefer pure python external libraries over platform dependent ones
- Use pytest for tests
- Use PEP8 (with maximal line length set to 120 characters)
- Use type hints

Here you are list of steps to set up and test sample gRPC service. 

```bash
# Make sure we will use correct python.
$ python --version
Python 3.6.8

# Create python virtual environment.
$ python -m venv venv

# Activate python virtual environment.
$ source venv/bin/activate

# Install project dependencies
(venv) $ pip install -r requirements.txt

# Build gRPC modules.
(venv) $ make proto
```

Run sample server:

```bash
(venv) $ python server.py
INFO:__main__:listening @ localhost:50051...
```

Run sample client to test communication with server:

```bash
(venv) $ python client.py "hello world"
hello world -> HELLO WORLD
```

Documentation and other resources:

- [gRPC](www.grpc.io)
- [Our Experience Designing and Building gRPC Services](https://dzone.com/articles/our-experience-designing-and-building-grpc-service)
- [Building APIs with gRPC](https://medium.com/google-cloud/building-apis-with-grpc-50842234aec8)
- [How We Build gRPC Services At Namely](https://medium.com/namely-labs/how-we-build-grpc-services-at-namely-52a3ae9e7c35)
- [Handy tool for file transfer using Python gRPC](https://github.com/gooooloo/grpc-file-transfer)
