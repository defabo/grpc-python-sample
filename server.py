import argparse
import logging
from concurrent import futures
from logging.handlers import RotatingFileHandler
from config import SampleConfig
import sys
import toml

import grpc

from sample_pb2 import ConvertReply, ConvertRequest
from sample_pb2_grpc import SampleServiceServicer, add_SampleServiceServicer_to_server

logger = logging.getLogger(__name__)


class SampleServicer(SampleServiceServicer):

    def convert(self, request: ConvertRequest, context: grpc.ServicerContext) -> ConvertReply:
        peer = context.peer()
        logger.info(f"convert called from {peer}")
        result = request.text.upper()
        return ConvertReply(text=result)


def serve(address: str) -> None:
    logging.basicConfig(level=logging.DEBUG)
    logger.info(f"listening @ {address}...")
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    add_SampleServiceServicer_to_server(SampleServicer(), server)
    server.add_insecure_port(address)
    server.start()
    server.wait_for_termination()


def main(args) -> None:
    try:
        config = SampleConfig(toml.load(args.config))
    except EnvironmentError as error:
        logging.error("can't open configuration: {0}".format(error))
        sys.exit(-1)
    except TypeError as error:
        logging.error("can't load configuration: {0}".format(error))
        sys.exit(-1)
    except toml.TomlDecodeError as error:
        logging.error("configuration error: {0}".format(error))
        sys.exit(-1)

    setup_logging(config.logs, config.debug)

    serve(config.address)


def setup_logging(logs: str, debug: bool) -> None:
    level = logging.DEBUG if debug else logging.INFO
    logging.basicConfig(level=level)
    if logs:
        root = logging.getLogger()
        handler = RotatingFileHandler(logs, maxBytes=1024*1024, backupCount=5)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(name)s %(message)s')
        handler.setFormatter(formatter)
        root.addHandler(handler)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Defabo gRPC sample server.')
    parser.add_argument('--config', dest='config', default="config.toml",
                        help='path to configuration file (default: %(default)s)')
    main(parser.parse_args())
