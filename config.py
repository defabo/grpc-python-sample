"""Configuration store."""


class SampleConfig:

    def __init__(self, config) -> None:
        self._config = config

    @property
    def address(self) -> str:
        d = self._config.get("app", {})
        return d.get("address")

    @property
    def logs(self) -> str:
        d = self._config.get("app", {})
        return d.get("logs", "console")

    @property
    def debug(self) -> bool:
        d = self._config.get("app", {})
        return d.get("debug", False)
